package controller

import (
	"bitbucket.org/liblabs/backend/pkg/model"
	"golang.org/x/oauth2"
)

type (
	OAuthService interface {
		VKToken(code string) (*oauth2.Token, error)
		GoogleToken(code string) (*oauth2.Token, error)
		VKUser(tok *oauth2.Token) (*model.User, error)
		GoogleUser(tok *oauth2.Token) (*model.User, error)
	}

	ItemStorage interface {
		ByCategory(category string, offset, limit int) ([]*model.Item, int, error)
		ByIDList(list []string, offset, limit int) ([]*model.Item, int, error)
		BySlug(slug string) (*model.Item, error)
		Add(items []*model.Item) error
		UpsertReview(review *model.Review) error
		ItemReviews(slug string, offset, limit int) ([]*model.Review, []*model.User, int, error)
		ItemScoreFromReviews(item string) (float32, error)
		Search(search string, offset, limit int) ([]*model.Item, int, error)
		IncCount(slug string, amount int) error
		SetScore(slug string, score float32) error
	}

	UserStorage interface {
		ByID(email string) (*model.User, error)
		Save(u model.User) error
		AddFavorite(id string, favorite string) error
		DeleteFavorite(id string, favorite string) error
		IncBalance(id string, amount int) error
		AddPaymentToken(token, userID string) error
		PaymentToken(id string) (*model.PaymentToken, error)
		RemovePaymentToken(token string) error
		SetQR(id string, qr string) error
	}

	CategoryStorage interface {
		List(offset, limit int) ([]*model.Category, int, error)
		Sub(name string, offset, limit int) ([]*model.Category, int, error)
		Add(categories []*model.Category) error
		AddSubcategories(name string, children []*model.Category) error
		Upsert(category model.Category) error
	}

	OrderStorage interface {
		AddOrder(order model.Order) error
		GetOrdersByUserID(id string, offset, limit int) ([]*model.Order, int, error)
	}

	PaymentService interface {
		RequestToken(o model.Points, c model.Customer) (*model.Token, error)
	}

	ImageService interface {
		UploadImage(bucket, path string, image []byte) error
		ImageURL(bucket, path string) string
	}

	Service struct {
		Contact
		Auth
		Item
		Category
		User
		Payment
		Order
	}
)

func NewService(
	is ItemStorage, us UserStorage, cs CategoryStorage, os OrderStorage,
	ps PaymentService, web OAuthService, mobile OAuthService, img ImageService) *Service {
	return &Service{
		Auth:     Auth{us, mobile, web, img},
		Item:     Item{is},
		Category: Category{cs},
		User:     User{is, us, img},
		Payment:  Payment{us, ps},
		Order:    Order{os, is, us},
		Contact:  Contact{},
	}
}
