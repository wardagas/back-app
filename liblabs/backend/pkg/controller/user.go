package controller

import (
	"fmt"

	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/satori/go.uuid"
	"github.com/skip2/go-qrcode"
)

type User struct {
	itemStorage ItemStorage
	userStorage UserStorage
	img         ImageService
}

func (u *User) Self(id string) (*model.User, error) {
	return u.userStorage.ByID(id)
}

func (u *User) FavoriteExists(id string, favorite string) (bool, error) {
	user, err := u.userStorage.ByID(id)
	if err != nil {
		return false, errors.Wrap(err, "load user")
	}

	for i := range user.Favorites {
		if user.Favorites[i] == favorite {
			return true, nil
		}
	}
	return false, nil
}

func (u *User) AddFavorite(id string, favorite string) error {
	return u.userStorage.AddFavorite(id, favorite)
}

func (u *User) DeleteFavorite(id string, favorite string) (*ItemPreview, error) {
	// Question: what to do if I've already removed item, but get error when get it by slug?
	// Use transactions or smth like that?
	err := u.userStorage.DeleteFavorite(id, favorite)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("delete favorite %q", favorite))
	}

	item, err := u.itemStorage.BySlug(favorite)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("get item %q", favorite))
	}
	return getItemPreview(item), nil
}

func (u *User) GetFavorites(id string, pageNum, pageSize int) (*model.ResultList, error) {
	user, err := u.userStorage.ByID(id)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("get user by id"))
	}

	items, total, err := u.itemStorage.ByIDList(user.Favorites, pageSize*(pageNum-1), pageSize)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("get favorite list"))
	}

	result := &model.ResultList{
		Results:    getItemPreviewList(items),
		TotalPages: totalPages(total, pageSize),
		Page:       pageNum,
	}
	return result, nil
}

func (u *User) GetBalance(id string) (int, error) {
	user, err := u.userStorage.ByID(id)
	if err != nil {
		return 0, fmt.Errorf("get user by id: %v", err)
	}
	return user.Balance, err
}

func (u *User) QR(id string) (string, error) {
	user, err := u.userStorage.ByID(id)
	if err != nil {
		return "", fmt.Errorf("get user by id: %v", err)
	}

	if user.QrURL == "" {
		// TODO save user to db.
		id, err := uuid.NewV4()
		if err != nil {
			return "", fmt.Errorf("generate user uuid: %v", err)
		}
		user.UUID = id.String()

		// Generate QR code.
		path := "qr/" + user.ID + ".png"
		qr, err := qrcode.Encode(user.UUID, qrcode.Medium, 256)
		if err != nil {
			return "", fmt.Errorf("generate qr code: %v", err)
		}
		if err := u.img.UploadImage(bucket, path, qr); err != nil {
			return "", fmt.Errorf("upload qr code: %v", err)
		}
		qrURL := u.img.ImageURL(bucket, path)
		user.QrURL = qrURL

		if err := u.userStorage.SetQR(user.ID, qrURL); err != nil {
			return "", err
		}
	}

	return user.QrURL, err
}
