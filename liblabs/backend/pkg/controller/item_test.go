package controller

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/liblabs/backend/pkg/mocks"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

var (
	items = []*model.Item{
		{
			Slug:        "new-book",
			Title:       "New book",
			ImageURL:    "https://dribble.com",
			Author:      "Me",
			Description: "Cool book",
			Category:    "fantasy",
			Year:        2018,
		},
		{
			Slug:        "oz-book",
			Title:       "OZ book",
			ImageURL:    "https://dribble.com",
			Author:      "Me",
			Description: "Cool book",
			Category:    "fantasy",
			Year:        2018,
		},
	}

	reviews = []*model.Review{
		{
			User:    user.ID,
			Item:    "new-book",
			Comment: "It was great!",
			Date:    time.Date(2018, time.November, 4, 0, 0, 0, 0, time.UTC),
		},
		{
			User:    user.ID,
			Item:    "new-book",
			Comment: "It was cool!",
			Date:    time.Date(2017, time.November, 4, 0, 0, 0, 0, time.UTC),
		},
	}

	user = model.User{
		ID:        "jfxer@gmail.com",
		Name:      "Siyar Akhmad",
		ImageURL:  "https://dribble.com",
		Favorites: []string{"new-book", "oz-book"},
		Balance:   5,
	}
)

func TestItem_ItemsByCategory(t *testing.T) {
	storage := new(mocks.ItemStorage)
	defer storage.AssertExpectations(t)
	storage.On("ByCategory", "fantasy", 0, 2).
		Return(items, total, nil).Once()
	storage.On("ByCategory", "fantasy", 0, 2).
		Return(nil, 0, fmt.Errorf("database: not found")).Once()

	controller := Item{storage}

	expectedResult := &model.ResultList{
		Results: []interface{}{
			&ItemPreview{
				Slug:     "new-book",
				Title:    "New book",
				ImageURL: "https://dribble.com",
			},
			&ItemPreview{
				Slug:     "oz-book",
				Title:    "OZ book",
				ImageURL: "https://dribble.com",
			},
		},
		TotalPages: totalPagesNum,
		Page:       pageNum,
	}

	gotResult, err := controller.ItemsByCategory("fantasy", pageNum, pageSize)
	assert.NoError(t, err, "want no error: get items by category")
	assert.Equal(t, expectedResult, gotResult)

	_, err = controller.ItemsByCategory("fantasy", pageNum, pageSize)
	assert.EqualError(t, err, "get items of fantasy: database: not found")
}

func TestItem_AddItemsByCategory(t *testing.T) {
	storage := new(mocks.ItemStorage)
	defer storage.AssertExpectations(t)
	storage.On("Add", items).Return(nil).Once()
	storage.On("Add", items).
		Return(fmt.Errorf("database: not found")).Once()

	controller := Item{storage}

	err := controller.AddItemsByCategory("fantasy", items)
	assert.NoError(t, err)

	err = controller.AddItemsByCategory("fantasy", items)
	assert.EqualError(t, err, "add items to fantasy: database: not found")
}

func TestItem_Item(t *testing.T) {
	storage := new(mocks.ItemStorage)
	defer storage.AssertExpectations(t)
	storage.On("BySlug", "new-book").Return(items[0], nil).Once()
	storage.On("BySlug", "new-book").
		Return(nil, fmt.Errorf("database: not found")).Once()

	controller := Item{storage}

	item, err := controller.Item("new-book")
	assert.NoError(t, err)
	assert.Equal(t, items[0], item)

	item, err = controller.Item("new-book")
	assert.EqualError(t, err, "database: not found")
	assert.Nil(t, item)
}

// TODO add score testing.
func TestItem_AddItemReview(t *testing.T) {
	storage := new(mocks.ItemStorage)
	defer storage.AssertExpectations(t)
	storage.On("UpsertReview", reviews[0]).Return(nil).Once()
	storage.On("ItemScoreFromReviews", reviews[0].Item).
		Return(float32(1), nil).Once()
	storage.On("SetScore", reviews[0].Item, float32(1)).
		Return(nil).Once()
	storage.On("UpsertReview", reviews[0]).
		Return(fmt.Errorf("database: not found")).Once()

	controller := Item{storage}

	err := controller.UpsertReview("new-book", reviews[0], user.ID)
	assert.NoError(t, err)

	err = controller.UpsertReview("new-book", reviews[0], user.ID)
	assert.EqualError(t, err, "add review to new-book: database: not found")
}

func TestItem_ItemReviews(t *testing.T) {
	users := []*model.User{&user, &user}

	storage := new(mocks.ItemStorage)
	defer storage.AssertExpectations(t)
	storage.On("ItemReviews", "new-book", 0, 2).
		Return(reviews, users, total, nil).Once()
	storage.On("ItemReviews", "new-book", 0, 2).
		Return(nil, nil, 0, fmt.Errorf("database: not found")).Once()

	controller := Item{storage}

	expectedResult := &model.ResultList{
		Results: []interface{}{
			&ReviewPreview{
				UserName:     user.Name,
				UserImageURL: user.ImageURL,
				Review:       reviews[0],
			},
			&ReviewPreview{
				UserName:     user.Name,
				UserImageURL: user.ImageURL,
				Review:       reviews[1],
			},
		},
		TotalPages: totalPagesNum,
		Page:       pageNum,
	}

	gotResult, err := controller.ItemReviews("new-book", pageNum, pageSize)
	assert.NoError(t, err)
	assert.Equal(t, expectedResult, gotResult)

	gotResult, err = controller.ItemReviews("new-book", pageNum, pageSize)
	assert.EqualError(t, err, "get reviews of new-book: database: not found")
	assert.Nil(t, gotResult)
}
