package controller

import (
	"fmt"

	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
)

type (
	Payment struct {
		storage UserStorage
		service PaymentService
	}
)

func (p *Payment) RequestPayment(id string) (string, error) {
	user, err := p.storage.ByID(id)
	if err != nil {
		return "", fmt.Errorf("get user by id: %v", err)
	}

	order := model.Points{
		Currency:    "BYN",
		Amount:      "500",
		Description: "Внутрення валюта LibLabs",
	}
	// TODO: Fix email and first, last name.
	customer := model.Customer{
		FirstName: user.Name,
		LastName:  "",
		Country:   "BY",
	}
	token, err := p.service.RequestToken(order, customer)
	if err != nil {
		return "", fmt.Errorf("request token for user %q: %v", user.ID, err)
	}

	if err := p.storage.AddPaymentToken(token.Token, user.ID); err != nil {
		return "", fmt.Errorf("add payment token: %v", err)
	}
	return token.RedirectURL, nil
}

func (p *Payment) HandlePayment(status, token string, amount int) error {
	// TODO remove payment token model and work only with user_id.
	// TODO add mongo transactions.
	// TODO add log if failed and declined.
	// TODO check expired token.
	if status == "successful" {
		t, err := p.storage.PaymentToken(token)
		if err != nil {
			return errors.Wrap(err, "get payment token")
		}

		if err := p.storage.IncBalance(t.User, amount); err != nil {
			return errors.Wrap(err, fmt.Sprintf("increase balance: %v", err))
		}
	}

	if err := p.storage.RemovePaymentToken(token); err != nil {
		return errors.Wrap(err, "remove payment token")
	}
	return nil
}
