package controller

import (
	"fmt"

	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/gosimple/slug"
)

type Category struct {
	storage CategoryStorage
}

func (c *Category) TopCategories(pageNum, pageSize int) (*model.ResultList, error) {
	categories, total, err := c.storage.List(pageSize*(pageNum-1), pageSize)
	if err != nil {
		return nil, errors.Wrap(err, "get top categories")
	}

	result := &model.ResultList{
		Results:    categories,
		TotalPages: totalPages(total, pageSize),
		Page:       pageNum,
	}
	return result, nil
}

func totalPages(total, pageSize int) int {
	// Question: is it bad or should be commented?
	if total == 0 {
		return 1
	}

	if total%pageSize == 0 {
		return total / pageSize
	}
	return total/pageSize + 1
}

func (c *Category) Subcategories(name string, pageNum, pageSize int) (*model.ResultList, error) {
	categories, total, err := c.storage.Sub(name, pageSize*(pageNum-1), pageSize)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("get subcategories %s", name))
	}

	result := &model.ResultList{
		Results:    categories,
		TotalPages: totalPages(total, pageSize),
		Page:       pageNum,
	}
	return result, nil
}

func (c *Category) AddCategories(categories []*model.Category) error {
	addSlugs(categories)

	if err := c.storage.Add(categories); err != nil {
		return errors.Wrap(err, "add categories")
	}
	return nil
}

func addSlugs(categories []*model.Category) {
	for i := range categories {
		categories[i].Slug = slug.Make(categories[i].Title)
	}
}

func (c *Category) AddSubCategories(parent string, categories []*model.Category) error {
	addSlugs(categories)

	if err := c.storage.AddSubcategories(parent, categories); err != nil {
		return errors.Wrap(err, fmt.Sprintf("add subcategories to %s", parent))
	}
	return nil
}

func (c *Category) UpsertBySlug(slug string, category model.Category) error {
	// TODO remove when json schema will be added.
	category.Slug = slug
	return c.storage.Upsert(category)
}
