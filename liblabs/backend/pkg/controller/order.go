package controller

import (
	"fmt"

	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
)

type (
	Order struct {
		orderStorage OrderStorage
		itemStorage  ItemStorage
		userStorage  UserStorage
	}
)

func (o *Order) AddOrder(id string, order model.Order) error {
	// Load.
	user, err := o.userStorage.ByID(id)
	if err != nil {
		return errors.Wrap(err, "user by id")
	}
	item, err := o.itemStorage.BySlug(order.Slug)
	if err != nil {
		return errors.Wrap(err, "item by slug")
	}

	// Apply logic.
	if item.AvailableCount <= 0 {
		return &errors.Error{
			Kind: errors.NotFound,
			Err:  fmt.Errorf("%s is not available", order.Slug),
		}
	}
	if user.Balance < 1 {
		return &errors.Error{
			Kind: errors.Conflict,
			Err:  fmt.Errorf("insufficient funds"),
		}
	}

	// Save.
	order.User = id
	order.Title = item.Title
	order.ImageURL = item.ImageURL
	order.Status = "pending"
	// TODO add mongo transactions.
	if err := o.itemStorage.IncCount(order.Slug, -1); err != nil {
		return errors.Wrap(err, "decrease item count")
	}
	if err := o.userStorage.IncBalance(user.ID, -1); err != nil {
		return errors.Wrap(err, "decrease balance")
	}
	if err := o.orderStorage.AddOrder(order); err != nil {
		return errors.Wrap(err, "add order")
	}
	return nil
}

func (o *Order) GetOrders(id string, pageNum, pageSize int) (*model.ResultList, error) {
	orders, total, err := o.orderStorage.GetOrdersByUserID(id, pageSize*(pageNum-1), pageSize)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("get orders of %s", id))
	}

	// TODO fix this
	if total == 0 {
		orders = make([]*model.Order, 0)
	}
	result := &model.ResultList{
		Results:    orders,
		TotalPages: totalPages(total, pageSize),
		Page:       pageNum,
	}
	return result, nil
}
