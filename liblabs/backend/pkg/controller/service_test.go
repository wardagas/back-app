package controller

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewService(t *testing.T) {
	service := &Service{
		Contact:  Contact{},
		Auth:     Auth{},
		Item:     Item{},
		Category: Category{},
		User:     User{},
	}

	assert.Equal(t, service, NewService(nil, nil, nil, nil,
		nil, nil, nil, nil))
}
