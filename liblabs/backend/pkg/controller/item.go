package controller

import (
	"fmt"
	"time"

	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/gosimple/slug"
)

type (
	Item struct {
		storage ItemStorage
	}

	ItemPreview struct {
		Slug     string `json:"slug"`
		Title    string `json:"title"`
		ImageURL string `json:"imageURL"`
	}

	ReviewPreview struct {
		UserName     string `json:"name"`
		UserImageURL string `json:"imageURL"`
		*model.Review
	}
)

func (i *Item) ItemsByCategory(name string, pageNum, pageSize int) (*model.ResultList, error) {
	items, total, err := i.storage.ByCategory(name, pageSize*(pageNum-1), pageSize)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("get items of %s", name))
	}

	result := &model.ResultList{
		Results:    getItemPreviewList(items),
		TotalPages: totalPages(total, pageSize),
		Page:       pageNum,
	}
	return result, nil
}

func (i *Item) ItemsSearch(search string, pageNum, pageSize int) (*model.ResultList, error) {
	items, total, err := i.storage.Search(search, pageSize*(pageNum-1), pageSize)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("search on %s", search))
	}

	result := &model.ResultList{
		Results:    getItemPreviewList(items),
		TotalPages: totalPages(total, pageSize),
		Page:       pageNum,
	}
	return result, nil
}

func getItemPreviewList(items []*model.Item) []interface{} {
	preview := make([]interface{}, len(items))

	for i := range items {
		preview[i] = getItemPreview(items[i])
	}
	return preview
}

func getItemPreview(item *model.Item) *ItemPreview {
	return &ItemPreview{
		item.Slug,
		item.Title,
		item.ImageURL,
	}
}

func (i *Item) AddItemsByCategory(category string, items []*model.Item) error {
	for i := range items {
		items[i].Slug = slug.Make(items[i].Title)
		items[i].Category = category
	}

	if err := i.storage.Add(items); err != nil {
		return errors.Wrap(err, fmt.Sprintf("add items to %s", category))
	}
	return nil
}

func (i *Item) Item(slug string) (*model.Item, error) {
	return i.storage.BySlug(slug)
}

func (i *Item) UpsertReview(name string, review *model.Review, userID string) error {
	review.Item = name
	review.User = userID
	review.Date = time.Now()

	if err := i.storage.UpsertReview(review); err != nil {
		return errors.Wrap(err, fmt.Sprintf("add review to %s", name))
	}

	avgScore, err := i.storage.ItemScoreFromReviews(name)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("get item score %s", name))
	}
	if err := i.storage.SetScore(name, avgScore); err != nil {
		return errors.Wrap(err, fmt.Sprintf("set average item score %s", name))
	}

	return nil
}

func (i *Item) ItemReviews(name string, pageNum, pageSize int) (*model.ResultList, error) {
	reviews, users, total, err := i.storage.ItemReviews(name, pageSize*(pageNum-1), pageSize)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("get reviews of %s", name))
	}

	result := &model.ResultList{
		Results:    getReviewPreview(reviews, users),
		TotalPages: totalPages(total, pageSize),
		Page:       pageNum,
	}
	return result, nil
}

func getReviewPreview(reviews []*model.Review, users []*model.User) []interface{} {
	preview := make([]interface{}, len(reviews))

	for i := range reviews {
		preview[i] = &ReviewPreview{
			users[i].Name,
			users[i].ImageURL,
			reviews[i],
		}

	}
	return preview
}
