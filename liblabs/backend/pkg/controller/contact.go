package controller

import "bitbucket.org/liblabs/backend/pkg/model"

type Contact struct{}

func (c *Contact) Shops() ([]*model.Shop, error) {
	shops := []*model.Shop{
		{
			Phone: "+375291724739",
			Time:  "08:00-23:00",
			Location: model.Location{
				Latitude:  53.914160,
				Longitude: 27.589800,
			},
		},
		{
			Phone: "+375297039373",
			Time:  "08:00-23:00",
			Location: model.Location{
				Latitude:  52.243301,
				Longitude: 26.788931,
			},
		},
	}
	return shops, nil
}
