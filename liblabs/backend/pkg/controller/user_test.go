package controller

import (
	"fmt"
	"testing"

	"bitbucket.org/liblabs/backend/pkg/mocks"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

func TestUser_ByEmail(t *testing.T) {
	storage := new(mocks.UserStorage)
	defer storage.AssertExpectations(t)
	storage.On("ByID", "jfxer@gmail.com").
		Return(&user, nil).Once()
	storage.On("ByID", "jfxer@gmail.com").
		Return(nil, fmt.Errorf("database: not found")).Once()

	controller := User{nil, storage, nil}
	gotUser, err := controller.Self("jfxer@gmail.com")
	assert.NoError(t, err)
	assert.Equal(t, user, *gotUser)

	gotUser, err = controller.Self("jfxer@gmail.com")
	assert.EqualError(t, err, "database: not found")
	assert.Nil(t, gotUser)
}

func TestUser_AddFavorite(t *testing.T) {
	storage := new(mocks.UserStorage)
	defer storage.AssertExpectations(t)
	storage.On("AddFavorite", "jfxer@gmail.com", "anna-karenina").
		Return(nil)

	controller := User{nil, storage, nil}
	err := controller.AddFavorite("jfxer@gmail.com", "anna-karenina")
	assert.NoError(t, err)
}

func TestUser_DeleteFavorite(t *testing.T) {
	item := &model.Item{
		Slug:        "anna-karenina",
		Title:       "Anna Karenina",
		ImageURL:    "https://dribble.com",
		Author:      "Me",
		Description: "Cool book",
		Category:    "fantasy",
		Year:        2018,
	}
	preview := &ItemPreview{
		Slug:     "anna-karenina",
		Title:    "Anna Karenina",
		ImageURL: "https://dribble.com",
	}

	storage := new(mocks.UserStorage)
	defer storage.AssertExpectations(t)
	storage.On("DeleteFavorite", "jfxer@gmail.com", "anna-karenina").
		Return(nil)

	itemStorage := new(mocks.ItemStorage)
	defer itemStorage.AssertExpectations(t)
	itemStorage.On("BySlug", "anna-karenina").
		Return(item, nil).Once()

	controller := User{itemStorage, storage, nil}
	gotPreview, err := controller.DeleteFavorite("jfxer@gmail.com", "anna-karenina")
	assert.NoError(t, err)
	assert.Equal(t, preview, gotPreview)
}

func TestUser_GetFavorites(t *testing.T) {
	userStorage := new(mocks.UserStorage)
	defer userStorage.AssertExpectations(t)
	userStorage.On("ByID", "jfxer@gmail.com").
		Return(nil, fmt.Errorf("database: not found")).Once()
	userStorage.On("ByID", "jfxer@gmail.com").
		Return(&user, nil)

	itemStorage := new(mocks.ItemStorage)
	defer itemStorage.AssertExpectations(t)
	itemStorage.On("ByIDList", []string{"new-book", "oz-book"}, 0, 2).
		Return(nil, 0, fmt.Errorf("database: not found")).Once()
	itemStorage.On("ByIDList", []string{"new-book", "oz-book"}, 0, 2).
		Return(items, total, nil)

	expectedResult := &model.ResultList{
		Results: []interface{}{
			&ItemPreview{
				Slug:     "new-book",
				Title:    "New book",
				ImageURL: "https://dribble.com",
			},
			&ItemPreview{
				Slug:     "oz-book",
				Title:    "OZ book",
				ImageURL: "https://dribble.com",
			},
		},
		TotalPages: totalPagesNum,
		Page:       pageNum,
	}

	controller := User{itemStorage, userStorage, nil}
	_, err := controller.GetFavorites("jfxer@gmail.com", pageNum, pageSize)
	assert.EqualError(t, err, "get user by id: database: not found")

	_, err = controller.GetFavorites("jfxer@gmail.com", pageNum, pageSize)
	assert.EqualError(t, err, "get favorite list: database: not found")

	gotResult, err := controller.GetFavorites("jfxer@gmail.com", pageNum, pageSize)
	assert.NoError(t, err)
	assert.Equal(t, expectedResult, gotResult)
}

func TestUser_FavoriteExists(t *testing.T) {
	storage := new(mocks.UserStorage)
	defer storage.AssertExpectations(t)
	storage.On("ByID", "jfxer@gmail.com").
		Return(nil, fmt.Errorf("not found")).Once()
	storage.On("ByID", "jfxer@gmail.com").
		Return(&user, nil)

	controller := User{nil, storage, nil}
	exists, err := controller.FavoriteExists("jfxer@gmail.com", "new-book")
	assert.EqualError(t, err, "load user: not found")
	assert.False(t, exists)

	exists, err = controller.FavoriteExists("jfxer@gmail.com", "new-book")
	assert.NoError(t, err)
	assert.True(t, exists)

	exists, err = controller.FavoriteExists("jfxer@gmail.com", "my-book")
	assert.NoError(t, err)
	assert.False(t, exists)
}

func TestUser_GetBalance(t *testing.T) {
	storage := new(mocks.UserStorage)
	defer storage.AssertExpectations(t)
	storage.On("ByID", "jfxer@gmail.com").
		Return(nil, fmt.Errorf("not found")).Once()
	storage.On("ByID", "jfxer@gmail.com").
		Return(&user, nil).Once()

	controller := User{nil, storage, nil}
	gotBalance, err := controller.GetBalance("jfxer@gmail.com")
	assert.EqualError(t, err, "get user by id: not found")
	assert.Zero(t, gotBalance)

	gotBalance, err = controller.GetBalance("jfxer@gmail.com")
	assert.NoError(t, err)
	assert.Equal(t, 5, gotBalance)
}
