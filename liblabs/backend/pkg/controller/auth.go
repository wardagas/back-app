package controller

import (
	"fmt"

	"bitbucket.org/liblabs/backend/pkg/auth"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/satori/go.uuid"
	"github.com/skip2/go-qrcode"
	"golang.org/x/oauth2"
)

const bucket = "lib-labs-data"

type Auth struct {
	storage UserStorage
	mobile  OAuthService
	web     OAuthService
	img     ImageService
}

func (c *Auth) WebLoginVK(token string) (model.JWT, error) {
	tok, err := c.web.VKToken(token)
	if err != nil {
		return "", err
	}

	user, err := c.web.VKUser(tok)
	if err != nil {
		return "", err
	}
	return c.loginUser(*user)
}

func (c *Auth) WebLoginGoogle(token string) (model.JWT, error) {
	tok, err := c.web.GoogleToken(token)
	if err != nil {
		return "", err
	}

	user, err := c.web.GoogleUser(tok)
	if err != nil {
		return "", err
	}
	return c.loginUser(*user)
}

func (c *Auth) MobileLoginVK(token string) (model.JWT, error) {
	tok := &oauth2.Token{AccessToken: token}
	user, err := c.mobile.VKUser(tok)
	if err != nil {
		return "", err
	}
	return c.loginUser(*user)
}

func (c *Auth) MobileLoginGoogle(token string) (model.JWT, error) {
	tok, err := c.mobile.GoogleToken(token)
	if err != nil {
		return "", err
	}

	user, err := c.mobile.GoogleUser(tok)
	if err != nil {
		return "", err
	}
	return c.loginUser(*user)
}

func (c *Auth) loginUser(user model.User) (model.JWT, error) {
	u, err := c.storage.ByID(user.ID)
	switch {
	case errors.IsNotFound(err):
		if err := c.registerUser(&user); err != nil {
			return "", err
		}
		u = &user
	case err != nil:
		return "", err
	}

	jwt, err := auth.JWT(*u)
	if err != nil {
		return "", err
	}
	return model.JWT(jwt), nil
}

func (c *Auth) registerUser(user *model.User) error {
	// TODO run save to s3 in goroutine?
	// TODO what about transactions?
	// Generate uuid number.
	id, err := uuid.NewV4()
	if err != nil {
		return fmt.Errorf("generate user uuid: %v", err)
	}
	user.UUID = id.String()

	// Generate QR code.
	path := "qr/" + user.ID + ".png"
	qr, err := qrcode.Encode(user.UUID, qrcode.Medium, 256)
	if err != nil {
		return fmt.Errorf("generate qr code: %v", err)
	}
	if err := c.img.UploadImage(bucket, path, qr); err != nil {
		return fmt.Errorf("upload qr code: %v", err)
	}
	qrURL := c.img.ImageURL(bucket, path)
	user.QrURL = qrURL

	if err := c.storage.Save(*user); err != nil {
		return err
	}
	return nil
}
