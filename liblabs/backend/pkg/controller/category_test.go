package controller

import (
	"fmt"
	"testing"

	"bitbucket.org/liblabs/backend/pkg/mocks"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

const (
	pageNum       = 1
	pageSize      = 2
	total         = 10
	totalPagesNum = 5
)

var categories = []*model.Category{
	{
		Slug:           "fantasy",
		Title:          "fantasy",
		ImageURL:       "http://dribble.com",
		HasSubcategory: true,
	},
	{
		Slug:           "science",
		Title:          "science",
		ImageURL:       "http://dribble.com",
		HasSubcategory: false,
	},
}

func TestCategory_TopCategories(t *testing.T) {
	storage := new(mocks.CategoryStorage)
	defer storage.AssertExpectations(t)
	storage.On("List", 0, 2).
		Return(categories, total, nil).Once()
	storage.On("List", 0, 2).
		Return(nil, 0, fmt.Errorf("database: not found")).Once()

	controller := Category{
		storage: storage,
	}

	expectedResult := &model.ResultList{
		Results:    categories,
		TotalPages: totalPagesNum,
		Page:       pageNum,
	}

	gotResult, err := controller.TopCategories(pageNum, pageSize)
	assert.NoError(t, err, "want no error: get top categories")
	assert.Equal(t, expectedResult, gotResult)

	_, err = controller.TopCategories(pageNum, pageSize)
	assert.EqualError(t, err, "get top categories: database: not found")
}

func TestCategory_Subcategories(t *testing.T) {
	storage := new(mocks.CategoryStorage)
	defer storage.AssertExpectations(t)
	storage.On("Sub", "fiction", 0, 2).
		Return(categories, total, nil).Once()
	storage.On("Sub", "fiction", 0, 2).
		Return(nil, 0, fmt.Errorf("database: not found")).Once()

	controller := Category{
		storage: storage,
	}

	expectedResult := &model.ResultList{
		Results:    categories,
		TotalPages: totalPagesNum,
		Page:       pageNum,
	}

	gotResult, err := controller.Subcategories("fiction", pageNum, pageSize)
	assert.NoError(t, err, "want no error: get sub categories")
	assert.Equal(t, expectedResult, gotResult)

	_, err = controller.Subcategories("fiction", pageNum, pageSize)
	assert.EqualError(t, err, "get subcategories fiction: database: not found")
}

func TestCategory_AddCategories(t *testing.T) {
	storage := new(mocks.CategoryStorage)
	defer storage.AssertExpectations(t)
	storage.On("Add", categories).Return(nil).Once()
	storage.On("Add", categories).Return(
		fmt.Errorf("database: not found")).Once()

	controller := Category{
		storage: storage,
	}

	err := controller.AddCategories(categories)
	assert.NoError(t, err)

	err = controller.AddCategories(categories)
	assert.EqualError(t, err, "add categories: database: not found")
}

func TestCategory_AddSubCategories(t *testing.T) {
	storage := new(mocks.CategoryStorage)
	defer storage.AssertExpectations(t)
	storage.On("AddSubcategories", "fantasy", categories).
		Return(nil).Once()
	storage.On("AddSubcategories", "fantasy", categories).
		Return(fmt.Errorf("database: not found")).Once()

	controller := Category{
		storage: storage,
	}

	err := controller.AddSubCategories("fantasy", categories)
	assert.NoError(t, err)

	err = controller.AddSubCategories("fantasy", categories)
	assert.EqualError(t, err, "add subcategories to fantasy: database: not found")
}

func TestCategory_UpsertBySlug(t *testing.T) {
	storage := new(mocks.CategoryStorage)
	defer storage.AssertExpectations(t)
	storage.On("Upsert", *categories[0]).
		Return(nil).Once()
	storage.On("Upsert", *categories[0]).
		Return(fmt.Errorf("database: not found")).Once()

	controller := Category{
		storage: storage,
	}

	err := controller.UpsertBySlug("fantasy", *categories[0])
	assert.NoError(t, err)

	err = controller.UpsertBySlug("fantasy", *categories[0])
	assert.EqualError(t, err, "database: not found")
}

func Test_totalPages(t *testing.T) {
	tt := []struct {
		name            string
		total, pageSize int
		expected        int
	}{
		{
			name:     "not found",
			total:    0,
			pageSize: 10,
			expected: 1,
		},
		{
			name:     "whole last page",
			total:    30,
			pageSize: 10,
			expected: 3,
		},
		{
			name:     "small last page",
			total:    27,
			pageSize: 10,
			expected: 3,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			got := totalPages(tc.total, tc.pageSize)
			assert.Equal(t, tc.expected, got)
		})
	}
}
