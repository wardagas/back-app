package controller

import (
	"testing"

	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

var shops = []*model.Shop{
	{
		Phone: "+375291724739",
		Time:  "08:00-23:00",
		Location: model.Location{
			Latitude:  53.914160,
			Longitude: 27.589800,
		},
	},
	{
		Phone: "+375297039373",
		Time:  "08:00-23:00",
		Location: model.Location{
			Latitude:  52.243301,
			Longitude: 26.788931,
		},
	},
}

func TestContact_Shops(t *testing.T) {
	controller := Contact{}
	gotShops, err := controller.Shops()
	assert.NoError(t, err)
	assert.Equal(t, shops, gotShops)
}
