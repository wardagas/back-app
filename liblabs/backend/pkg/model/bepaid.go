package model

type (
	Token struct {
		Token       string `json:"token"`
		RedirectURL string `json:"redirect_url"`
	}

	Points struct {
		Currency    string `json:"currency"`
		Amount      string `json:"amount"`
		Description string `json:"description"`
	}

	Customer struct {
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Email     string `json:"email,omitempty"`
		Country   string `json:"country"`
	}
)
