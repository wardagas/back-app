package model

type (
	ResultList struct {
		Results    interface{} `json:"results"`
		TotalPages int         `json:"totalPages"`
		Page       int         `json:"page"`
	}
)
