package model

import (
	"fmt"
	"strings"
	"time"
)

type (
	User struct {
		// Question: is it bad to pass vk ID in jwt?
		ID        string   `json:"-" bson:"_id"`
		UUID      string   `json:"-" bson:"uuid"`
		QrURL     string   `json:"-" bson:"qr"`
		Name      string   `json:"name" bson:"name"`
		ImageURL  string   `json:"imageURL" bson:"image_url"`
		Balance   int      `json:"balance" bson:"balance"`
		Favorites []string `json:"-" bson:"favorites,omitempty"`
	}

	Category struct {
		Slug           string `json:"slug" bson:"_id"`
		Title          string `json:"title" bson:"title"`
		ImageURL       string `json:"imageURL" bson:"image_url"`
		HasSubcategory bool   `json:"hasSubcategory" bson:"-"`
	}

	Item struct {
		Slug        string `json:"slug" bson:"_id"`
		Category    string `json:"-" bson:"category_id"`
		Title       string `json:"title" bson:"title"`
		ImageURL    string `json:"imageURL" bson:"image_url"`
		Author      string `json:"author" bson:"author"`
		Description string `json:"description" bson:"description"`
		// TODO rename bson average score
		AverageScore   float32 `json:"averageScore" bson:"average_score"`
		AvailableCount int     `json:"availableCount" bson:"available_count"`
		Year           int     `json:"year" bson:"year"`
	}

	Info struct {
		Description string `json:"description" bson:"description"`
	}

	Shop struct {
		Phone string `json:"phone"`
		Time  string `json:"time"`
		Location
	}

	Location struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	}

	Review struct {
		User    string    `json:"-" bson:"user_id"`
		Item    string    `json:"-" bson:"item_id"`
		Title   string    `json:"title" bson:"title"`
		Comment string    `json:"comment" bson:"comment"`
		Score   int       `json:"score" bson:"score"`
		Date    time.Time `json:"date" bson:"date"`
	}

	PaymentToken struct {
		Token string `bson:"_id"`
		User  string `bson:"user_id"`
	}

	Order struct {
		User     string    `json:"-" bson:"user_id"`
		Slug     string    `json:"slug" bson:"slug"`
		Title    string    `json:"title" bson:"title"`
		ImageURL string    `json:"imageURL" bson:"image_url"`
		Date     OrderTime `json:"date" bson:"date"`
		Status   string    `json:"status" bson:"status"`
	}

	OrderStatus int
)

const format = "02.01.2006"

// TODO move to time package.
type OrderTime struct {
	time.Time
}

func (o *OrderTime) UnmarshalJSON(p []byte) error {
	t, err := time.Parse(format, strings.Replace(
		string(p),
		"\"",
		"",
		-1,
	))
	if err != nil {
		return fmt.Errorf("parse order time: %v", err)
	}
	o.Time = t
	return nil
}

func (o *OrderTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", o.Time.Format(format))
	return []byte(stamp), nil
}
