package model

type (
	// Question: which structures should use third-party packages?
	// Ex. auth uses model.JWT?
	// So, if I declare it in auth I can't use it in interface.
	JWT string
)
