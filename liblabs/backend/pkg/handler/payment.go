package handler

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/controller"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/handler/middleware"
)

type (
	paymentHandler struct {
		controller controller.Payment
	}

	Hook struct {
		Transaction struct {
			Status         string `json:"status"`
			AdditionalData struct {
				Vendor struct {
					Token string `json:"token"`
				} `json:"vendor"`
			} `json:"additional_data"`
		} `json:"transaction"`
	}

	redirect struct {
		Redirect string `json:"url"`
	}
)

func (p *paymentHandler) BuyPoints(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	redirectURL, err := p.controller.RequestPayment(user.ID)
	if err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}

	jsonResponse(w, redirect{redirectURL})
}

func (p *paymentHandler) Hook(w http.ResponseWriter, r *http.Request) {
	// TODO fix to struct or what is better?
	var request Hook
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
		return
	}

	token := request.Transaction.AdditionalData.Vendor.Token
	status := request.Transaction.Status
	if err := p.controller.HandlePayment(status, token, 5); err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
	}
}
