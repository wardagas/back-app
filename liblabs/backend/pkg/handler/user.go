package handler

import (
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/controller"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/handler/middleware"
	"github.com/gorilla/mux"
)

type (
	userHandler struct {
		controller controller.User
	}
)

func (u *userHandler) UserByEmail(w http.ResponseWriter, r *http.Request) {
	uc, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	user, err := u.controller.Self(uc.ID)
	if err != nil {
		errors.WriteJSON(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonResponse(w, user)
}

func (u *userHandler) AddFavorite(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	if name, ok := mux.Vars(r)["name"]; ok {
		err := u.controller.AddFavorite(user.ID, name)
		if err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}
		return
	}

	errors.WriteJSON(w, "favorite name missing", http.StatusBadRequest)
}

func (u *userHandler) FavoriteExists(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	if name, ok := mux.Vars(r)["name"]; ok {
		exists, err := u.controller.FavoriteExists(user.ID, name)
		if err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}

		if !exists {
			w.WriteHeader(http.StatusNotFound)
		}
		return
	}

	errors.WriteJSON(w, "favorite name missing", http.StatusBadRequest)
}

func (u *userHandler) DeleteFavorite(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	if name, ok := mux.Vars(r)["name"]; ok {
		item, err := u.controller.DeleteFavorite(user.ID, name)
		if err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}

		jsonResponse(w, item)
		return
	}

	errors.WriteJSON(w, "favorite name missing", http.StatusBadRequest)
}

func (u *userHandler) GetFavorites(w http.ResponseWriter, r *http.Request) {
	page, pageSize, ok := middleware.Page(r.Context())
	if !ok {
		errors.WriteJSON(w, "get page and page_size from context", http.StatusInternalServerError)
		return
	}

	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	items, err := u.controller.GetFavorites(user.ID, page, pageSize)
	if err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}

	jsonResponse(w, items)
}

func (u *userHandler) Balance(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	balance, err := u.controller.GetBalance(user.ID)
	if err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}

	var result = struct {
		Balance int `json:"balance"`
	}{
		Balance: balance,
	}
	jsonResponse(w, &result)
}

func (u *userHandler) QR(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	qr, err := u.controller.QR(user.ID)
	if err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}

	var result = struct {
		QR string `json:"url"`
	}{
		QR: qr,
	}
	jsonResponse(w, &result)
}
