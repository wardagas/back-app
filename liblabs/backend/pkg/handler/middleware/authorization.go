package middleware

import (
	"context"
	"crypto/subtle"
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/auth"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/dgrijalva/jwt-go/request"
)

type contextKey string

var (
	contextKeyUser = contextKey("user")
)

func JWT(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := request.ParseFromRequest(r, request.OAuth2Extractor,
			auth.KeyJWT())

		if err != nil || !token.Valid {
			errors.WriteJSON(w, err.Error(), http.StatusUnauthorized)
			return
		}

		user, err := auth.UserFromJWT(token)
		if err != nil {
			errors.WriteJSON(w,
				errors.Wrap(err, "can't get user from jwt token").Error(),
				http.StatusInternalServerError)
			return
		}
		ctx := r.Context()
		ctx = context.WithValue(ctx, contextKeyUser, user)

		next(w, r.WithContext(ctx))
	})
}

func User(ctx context.Context) (model.User, bool) {
	user, ok := ctx.Value(contextKeyUser).(model.User)
	return user, ok
}

func BasicAuth(realm, username, password string) func(http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			// TODO add hashing for basic authentication.
			user, pass, ok := r.BasicAuth()

			if !ok ||
				subtle.ConstantTimeCompare([]byte(user), []byte(username)) != 1 ||
				subtle.ConstantTimeCompare([]byte(pass), []byte(password)) != 1 {
				w.Header().Set("WWW-Authenticate", `Basic realm="`+realm+`"`)
				w.WriteHeader(http.StatusUnauthorized)
				return
			}

			next(w, r)
		}
	}
}
