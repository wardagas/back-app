package middleware

import (
	"net/http"

	"github.com/gorilla/handlers"
)

// Question: where to put cors config?
type (
	CORSConfig struct {
		Credentials bool
		Origins     []string
		Methods     []string
		Headers     []string
	}

	middleware func(http.HandlerFunc) http.HandlerFunc
)

func CORS(config CORSConfig) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		var opts []handlers.CORSOption
		if config.Credentials {
			opts = append(opts, handlers.AllowCredentials())
		}

		opts = append(opts, handlers.AllowedOrigins(config.Origins))
		opts = append(opts, handlers.AllowedMethods(config.Methods))
		opts = append(opts, handlers.AllowedHeaders(config.Headers))
		return handlers.CORS(opts...)(next)
	}
}

func Chain(f http.HandlerFunc, mw ...middleware) http.HandlerFunc {
	for _, m := range mw {
		f = m(f)
	}

	return f
}
