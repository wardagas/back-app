package middleware

import (
	"context"
	"net/http"
	"strconv"

	"bitbucket.org/liblabs/backend/pkg/errors"
)

var (
	contextKeyPageNum  = contextKey("pageNum")
	contextKeyPageSize = contextKey("pageSize")
)

func Pagination(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		page := 1
		pageSize := 10

		if r.FormValue("page") != "" {
			page, err = strconv.Atoi(r.FormValue("page"))
			if err != nil {
				errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
				return
			}
		}

		if r.FormValue("page_size") != "" {
			pageSize, err = strconv.Atoi(r.FormValue("page_size"))
			if err != nil {
				errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
				return
			}
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, contextKeyPageNum, page)
		ctx = context.WithValue(ctx, contextKeyPageSize, pageSize)

		next(w, r.WithContext(ctx))
	}
}

// Page gets page and page_size parameters from context.
func Page(ctx context.Context) (int, int, bool) {
	page, ok := ctx.Value(contextKeyPageNum).(int)
	if !ok {
		return 0, 0, false
	}

	pageSize, ok := ctx.Value(contextKeyPageSize).(int)
	if !ok {
		return 0, 0, false
	}
	return page, pageSize, true
}
