package handler

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/controller"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/handler/middleware"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/gorilla/mux"
)

type (
	itemHandler struct {
		controller controller.Item
	}
)

func (i *itemHandler) ItemsByCategory(w http.ResponseWriter, r *http.Request) {
	page, pageSize, ok := middleware.Page(r.Context())
	if !ok {
		errors.WriteJSON(w, "get page and page_size from context", http.StatusInternalServerError)
		return
	}

	if name, ok := mux.Vars(r)["name"]; ok {
		items, err := i.controller.ItemsByCategory(name, page, pageSize)
		if err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}

		jsonResponse(w, items)
		return
	}

	errors.WriteJSON(w, "category name missing", http.StatusBadRequest)
}

func (i *itemHandler) AddItemsByCategory(w http.ResponseWriter, r *http.Request) {
	if name, ok := mux.Vars(r)["name"]; ok {
		var items []*model.Item
		if err := json.NewDecoder(r.Body).Decode(&items); err != nil {
			errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
			return
		}

		if err := i.controller.AddItemsByCategory(name, items); err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}
		return
	}

	errors.WriteJSON(w, "category name missing", http.StatusBadRequest)
}

func (i *itemHandler) ItemBySlug(w http.ResponseWriter, r *http.Request) {
	if name, ok := mux.Vars(r)["name"]; ok {
		item, err := i.controller.Item(name)
		if err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}

		jsonResponse(w, item)
		return
	}

	errors.WriteJSON(w, "item name missing", http.StatusBadRequest)
}

func (i *itemHandler) UpsertReview(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	if name, ok := mux.Vars(r)["name"]; ok {
		var review *model.Review
		if err := json.NewDecoder(r.Body).Decode(&review); err != nil {
			errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
			return
		}

		if err := i.controller.UpsertReview(name, review, user.ID); err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}
		return
	}

	errors.WriteJSON(w, "item name missing", http.StatusBadRequest)
}

func (i *itemHandler) ItemReviews(w http.ResponseWriter, r *http.Request) {
	page, pageSize, ok := middleware.Page(r.Context())
	if !ok {
		errors.WriteJSON(w, "get page and page_size from context", http.StatusInternalServerError)
		return
	}

	if name, ok := mux.Vars(r)["name"]; ok {
		item, err := i.controller.ItemReviews(name, page, pageSize)
		if err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}

		jsonResponse(w, item)
		return
	}

	errors.WriteJSON(w, "item name missing", http.StatusBadRequest)
}

func (i *itemHandler) Search(w http.ResponseWriter, r *http.Request) {
	page, pageSize, ok := middleware.Page(r.Context())
	if !ok {
		errors.WriteJSON(w, "get page and page_size from context", http.StatusInternalServerError)
		return
	}

	search := mux.Vars(r)["search"]
	items, err := i.controller.ItemsSearch(search, page, pageSize)
	if err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}
	jsonResponse(w, items)
}
