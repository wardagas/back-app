package handler

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/controller"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/handler/middleware"
	"bitbucket.org/liblabs/backend/pkg/model"
)

type (
	orderHandler struct {
		controller controller.Order
	}
)

func (o *orderHandler) AddOrder(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	var order model.Order
	if err := json.NewDecoder(r.Body).Decode(&order); err != nil {
		errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := o.controller.AddOrder(user.ID, order); err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
	}
}

func (o *orderHandler) GetOrders(w http.ResponseWriter, r *http.Request) {
	user, ok := middleware.User(r.Context())
	if !ok {
		errors.WriteJSON(w, "get user from context", http.StatusInternalServerError)
		return
	}

	page, pageSize, ok := middleware.Page(r.Context())
	if !ok {
		errors.WriteJSON(w, "get page and page_size from context", http.StatusInternalServerError)
		return
	}

	// TODO remove slug in order.
	orders, err := o.controller.GetOrders(user.ID, page, pageSize)
	if err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}

	jsonResponse(w, orders)
}
