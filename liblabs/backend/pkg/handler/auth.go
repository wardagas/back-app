package handler

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/controller"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/gorilla/mux"
)

type (
	authHandler struct {
		controller controller.Auth
	}

	token struct {
		Token string `json:"token"`
	}
)

func (s *authHandler) Login(w http.ResponseWriter, r *http.Request) {
	var request token
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
		return
	}

	var (
		jwt model.JWT
		err error
	)

	oauthService := mux.Vars(r)["service"]
	client := mux.Vars(r)["client"]

	switch oauthService {
	case "google":
		switch client {
		case "web":
			jwt, err = s.controller.WebLoginGoogle(request.Token)
		case "mobile":
			jwt, err = s.controller.MobileLoginGoogle(request.Token)
		default:
			errors.WriteJSON(w, "unknown authorization type", http.StatusBadRequest)
			return
		}
	case "vk":
		switch client {
		case "web":
			jwt, err = s.controller.WebLoginVK(request.Token)
		case "mobile":
			jwt, err = s.controller.MobileLoginVK(request.Token)
		default:
			errors.WriteJSON(w, "unknown authorization type", http.StatusBadRequest)
			return
		}
	default:
		errors.WriteJSON(w, "unknown authorization type", http.StatusBadRequest)
		return
	}

	if err != nil {
		errors.WriteJSON(w, err.Error(), http.StatusUnauthorized)
		return
	}
	jsonResponse(w, token{string(jwt)})
}
