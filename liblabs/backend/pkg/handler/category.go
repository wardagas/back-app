package handler

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/controller"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/handler/middleware"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/gorilla/mux"
)

type (
	categoryHandler struct {
		controller controller.Category
	}
)

func (c *categoryHandler) TopCategories(w http.ResponseWriter, r *http.Request) {
	page, pageSize, ok := middleware.Page(r.Context())
	if !ok {
		errors.WriteJSON(w, "get page and page_size from context", http.StatusInternalServerError)
		return
	}

	categories, err := c.controller.TopCategories(page, pageSize)
	if err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}

	jsonResponse(w, categories)
}

func (c *categoryHandler) Subcategories(w http.ResponseWriter, r *http.Request) {
	page, pageSize, ok := middleware.Page(r.Context())
	if !ok {
		errors.WriteJSON(w, "get page and page_size from context", http.StatusInternalServerError)
		return
	}

	if name, ok := mux.Vars(r)["name"]; ok {
		category, err := c.controller.Subcategories(name, page, pageSize)
		if err != nil {
			errors.WriteJSON(w, err.Error(), http.StatusInternalServerError)
			return
		}

		jsonResponse(w, category)
		return
	}

	errors.WriteJSON(w, "category name missing", http.StatusBadRequest)
}

func (c *categoryHandler) AddCategories(w http.ResponseWriter, r *http.Request) {
	var categories []*model.Category
	// Question: The server doesn't need to close the body?
	if err := json.NewDecoder(r.Body).Decode(&categories); err != nil {
		errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := c.controller.AddCategories(categories); err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}
}

func (c *categoryHandler) AddSubCategories(w http.ResponseWriter, r *http.Request) {
	if name, ok := mux.Vars(r)["name"]; ok {
		var categories []*model.Category
		if err := json.NewDecoder(r.Body).Decode(&categories); err != nil {
			errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
			return
		}

		if err := c.controller.AddSubCategories(name, categories); err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}
		return
	}

	errors.WriteJSON(w, "category name missing", http.StatusBadRequest)
}

func (c *categoryHandler) Upsert(w http.ResponseWriter, r *http.Request) {
	if name, ok := mux.Vars(r)["name"]; ok {
		category := new(model.Category)
		if err := json.NewDecoder(r.Body).Decode(category); err != nil {
			errors.WriteJSON(w, err.Error(), http.StatusBadRequest)
			return
		}

		if err := c.controller.UpsertBySlug(name, *category); err != nil {
			errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
			return
		}
		return
	}

	errors.WriteJSON(w, "category name missing", http.StatusBadRequest)
}
