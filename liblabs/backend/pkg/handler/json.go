package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/errors"
)

func jsonResponse(w http.ResponseWriter, v interface{}) {
	w.Header().Set("Content-Type", "application/json")

	err := json.NewEncoder(w).Encode(v)
	if err != nil {
		errors.WriteJSON(w, fmt.Sprintf("encode %s as json: %v", v, err), http.StatusInternalServerError)
		return
	}
}
