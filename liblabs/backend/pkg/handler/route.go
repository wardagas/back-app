package handler

import (
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/bepaid"
	"bitbucket.org/liblabs/backend/pkg/controller"
	"bitbucket.org/liblabs/backend/pkg/handler/middleware"
	"github.com/gorilla/mux"
)

type Handler struct {
	contactHandler
	authHandler
	itemHandler
	userHandler
	categoryHandler
	paymentHandler
	orderHandler
}

func New(c *controller.Service) *Handler {
	h := &Handler{
		authHandler:     authHandler{c.Auth},
		itemHandler:     itemHandler{c.Item},
		userHandler:     userHandler{c.User},
		categoryHandler: categoryHandler{c.Category},
		contactHandler:  contactHandler{c.Contact},
		paymentHandler:  paymentHandler{c.Payment},
		orderHandler:    orderHandler{c.Order},
	}
	return h
}

func (h *Handler) SetRouter(c middleware.CORSConfig, b bepaid.Config) http.Handler {
	r := mux.NewRouter().StrictSlash(true)
	h.setCategory(r)
	h.setItem(r)
	h.setUser(r)
	h.setAuth(r)
	h.setService(r)
	h.setPayment(r, b)

	return middleware.CORS(c)(r)
}

func (h *Handler) setAuth(r *mux.Router) {
	r.HandleFunc("/auth/{service}/{client}", h.Login).Methods("POST")
}

func (h *Handler) setCategory(r *mux.Router) {
	c := r.PathPrefix("/categories").Subrouter()
	c.HandleFunc("", middleware.Pagination(h.TopCategories)).Methods("GET")
	c.HandleFunc("", h.AddCategories).Methods("POST")

	// Question: whether check with [a-z0-9]+(?:[-|_][a-z0-9]+)* or simpler regex?
	c.HandleFunc("/{name}",
		middleware.Pagination(h.Subcategories)).Methods("GET")
	c.HandleFunc("/{name}", h.AddSubCategories).Methods("POST")
	c.HandleFunc("/{name}", h.Upsert).Methods("PUT")

	c.HandleFunc("/{name}/items",
		middleware.Pagination(h.ItemsByCategory)).Methods("GET")
	c.HandleFunc("/{name}/items", h.AddItemsByCategory).Methods("POST")
}

func (h *Handler) setItem(r *mux.Router) {
	i := r.PathPrefix("/items").Subrouter()
	i.HandleFunc("",
		middleware.Pagination(h.Search)).
		Queries("search", "{search:.+}").
		Methods("GET")

	i.HandleFunc("/{name}", h.ItemBySlug).
		Methods("GET")

	i.HandleFunc("/{name}/reviews", middleware.Pagination(h.ItemReviews)).
		Methods("GET")
	i.HandleFunc("/{name}/reviews", middleware.JWT(h.UpsertReview)).
		Methods("PUT")
}

func (h *Handler) setUser(r *mux.Router) {
	r.HandleFunc("/self", middleware.JWT(h.UserByEmail)).
		Methods("GET")

	r.HandleFunc("/favorites",
		middleware.Chain(h.GetFavorites, middleware.Pagination, middleware.JWT)).
		Methods("GET")
	r.HandleFunc("/favorites/{name}",
		middleware.JWT(h.FavoriteExists)).
		Methods("GET")
	r.HandleFunc("/favorites/{name}", middleware.JWT(h.AddFavorite)).
		Methods("POST")
	r.HandleFunc("/favorites/{name}", middleware.JWT(h.DeleteFavorite)).
		Methods("DELETE")

	r.HandleFunc("/qr", middleware.JWT(h.QR)).
		Methods("GET")
	r.HandleFunc("/balance", middleware.JWT(h.Balance)).
		Methods("GET")
	r.HandleFunc("/orders", middleware.JWT(h.AddOrder)).
		Methods("POST")
	r.HandleFunc("/orders",
		middleware.Chain(h.GetOrders, middleware.Pagination, middleware.JWT)).
		Methods("GET")
}

func (h *Handler) setService(r *mux.Router) {
	r.HandleFunc("/info", h.Info).Methods("GET")
	r.HandleFunc("/shops", h.Shops).Methods("GET")
}

func (h *Handler) setPayment(r *mux.Router, config bepaid.Config) {
	r.HandleFunc("/payment", middleware.JWT(h.BuyPoints)).
		Methods("POST")
	r.HandleFunc("/payment/hook",
		middleware.BasicAuth("hook", config.ID, config.SecretKey)(h.Hook)).
		Methods("POST")
}
