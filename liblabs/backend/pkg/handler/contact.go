package handler

import (
	"net/http"

	"bitbucket.org/liblabs/backend/pkg/controller"
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
)

type (
	contactHandler struct {
		controller controller.Contact
	}
)

func (c *contactHandler) Info(w http.ResponseWriter, r *http.Request) {
	info := model.Info{
		Description: `
Liblabs - сервис для аренды книг.
Наша цель - новые книги для всех.
Здесь вы можете взять новую книгу или настольную игру на неделю за 1 балл
Если вы не нашли книгу или настольную игру, которую искали, то оставьте на нее заявку и в ближайшее время она появится.`,
	}
	jsonResponse(w, info)
}

func (c *contactHandler) Shops(w http.ResponseWriter, r *http.Request) {
	shops, err := c.controller.Shops()
	if err != nil {
		errors.WriteJSON(w, err.Error(), errors.HTTPStatus(err))
		return
	}

	jsonResponse(w, shops)
}
