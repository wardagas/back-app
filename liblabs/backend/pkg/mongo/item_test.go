package mongo

import (
	"testing"

	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

func TestItemService(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	is := storage.ItemService
	defer dropDatabase()

	items := []*model.Item{
		{
			Slug:        "new-book",
			Title:       "New book",
			ImageURL:    "https://dribble.com",
			Author:      "Me",
			Description: "Cool book",
			Category:    "fantasy",
			Year:        2018,
		},
		{
			Slug:        "oz-book",
			Title:       "OZ book",
			ImageURL:    "https://dribble.com",
			Author:      "Me",
			Description: "Cool book",
			Category:    "fantasy",
			Year:        2018,
		},
	}

	_, err = is.BySlug("new-book")
	assert.EqualError(t, err, "database: not found")

	err = is.Add(items)
	assert.NoError(t, err, "want no errors: add items")

	err = is.Add(items)
	assert.EqualError(t, err,
		"database: E11000 duplicate key error collection: "+
			"test.item index: _id_ dup key: { : \"new-book\" }")

	gotItems, n, err := is.ByCategory("fantasy", 0, 2)
	assert.NoError(t, err, "want no errors: get items by category")
	assert.Equal(t, items, gotItems)
	assert.Equal(t, 2, n)

	gotItem, err := is.BySlug("new-book")
	assert.NoError(t, err)
	assert.Equal(t, items[0], gotItem)

	gotItems, n, err = is.ByIDList([]string{"new-book", "oz-book"}, 0, 2)
	assert.NoError(t, err, "want no errors: get items by id list")
	assert.Equal(t, items, gotItems)
	assert.Equal(t, 2, n)
}

func TestItemService_Search(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	is := storage.ItemService
	defer dropDatabase()

	items := []*model.Item{
		{
			Slug:        "cool-book",
			Title:       "New cool book",
			ImageURL:    "https://dribble.com",
			Author:      "Me",
			Description: "Cool book",
			Category:    "fantasy",
			Year:        2018,
		},
		{
			Slug:        "me",
			Title:       "Me",
			ImageURL:    "https://dribble.com",
			Author:      "Somebody",
			Description: "Cool book",
			Category:    "fantasy",
			Year:        2018,
		},
		{
			Slug:        "oz-book",
			Title:       "OZ book",
			ImageURL:    "https://dribble.com",
			Author:      "Hey Men",
			Description: "New",
			Category:    "fantasy",
			Year:        2018,
		},
	}

	err = is.Add(items)
	assert.NoError(t, err, "want no errors: add items")

	gotItems, n, err := is.Search("cool book", 0, 3)
	assert.NoError(t, err, "want no errors: get items by category")
	assert.Equal(t, items[:2], gotItems)
	assert.Equal(t, 2, n)
}
