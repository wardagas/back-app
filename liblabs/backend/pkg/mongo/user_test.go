package mongo

import (
	"sort"
	"testing"

	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

func TestUserService(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	defer dropDatabase()

	_, err = storage.UserService.ByID("new@yandex.ru")
	assert.EqualError(t, err, "database: not found")

	user := model.User{
		ID:       "kat6123@yandex.ru",
		Name:     "Katerina",
		ImageURL: "http://dribble.com",
	}

	err = storage.UserService.Save(user)
	assert.NoError(t, err, "want no errors: insert user")

	gotUser, err := storage.UserService.ByID("kat6123@yandex.ru")
	assert.NoError(t, err, "want no errors: get user by email")
	assert.Equal(t, user, *gotUser)
}

func TestUserService_AddFavorites(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	defer dropDatabase()

	user := model.User{
		ID:       "kat6123@yandex.ru",
		Name:     "Katerina",
		ImageURL: "http://dribble.com",
	}
	favorites := []string{"anna-karenina", "garri-potter", "war-and-peace"}

	err = storage.UserService.Save(user)
	assert.NoError(t, err)

	for i := range favorites {
		err := storage.UserService.AddFavorite(user.ID, favorites[i])
		assert.NoError(t, err)
	}

	gotUser, err := storage.UserService.ByID(user.ID)
	assert.NoError(t, err)
	// Mongo returns not ordering set. Therefore it's sorted to be tested.
	sort.Strings(gotUser.Favorites)
	assert.Equal(t, favorites, gotUser.Favorites)

	err = storage.UserService.DeleteFavorite(user.ID, "war-and-peace")
	assert.NoError(t, err)

	gotUser, err = storage.UserService.ByID(user.ID)
	assert.NoError(t, err)
	sort.Strings(gotUser.Favorites)
	assert.Equal(t, []string{"anna-karenina", "garri-potter"}, gotUser.Favorites)
}

func TestUserService_IncBalance(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	defer dropDatabase()

	user := model.User{
		ID:       "kat6123@yandex.ru",
		Name:     "Katerina",
		ImageURL: "http://dribble.com",
		Balance:  0,
	}

	err = storage.UserService.Save(user)
	assert.NoError(t, err)

	gotUser, err := storage.UserService.ByID("kat6123@yandex.ru")
	assert.NoError(t, err)
	assert.Equal(t, gotUser.Balance, 0)

	err = storage.UserService.IncBalance("kat6123@yandex.ru", 50)
	assert.NoError(t, err)

	gotUser, err = storage.UserService.ByID("kat6123@yandex.ru")
	assert.NoError(t, err)
	assert.Equal(t, gotUser.Balance, 50)
}

func TestUserService_PaymentToken(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	defer dropDatabase()

	user := model.User{
		ID:       "kat6123@yandex.ru",
		Name:     "Katerina",
		ImageURL: "http://dribble.com",
		Balance:  0,
	}
	token := model.PaymentToken{
		Token: "adjhakdjhak2131871g87g9o87od7wq8",
		User:  user.ID,
	}

	err = storage.UserService.Save(user)
	assert.NoError(t, err)

	err = storage.UserService.AddPaymentToken(token.Token, user.ID)
	assert.NoError(t, err)

	gotToken, err := storage.UserService.PaymentToken(token.Token)
	assert.NoError(t, err)
	assert.Equal(t, user.ID, gotToken.User)

	err = storage.UserService.RemovePaymentToken(token.Token)
	assert.NoError(t, err)

	gotToken, err = storage.UserService.PaymentToken(token.Token)
	assert.EqualError(t, err, "database: not found")
	assert.Empty(t, gotToken)
}
