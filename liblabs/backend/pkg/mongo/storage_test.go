package mongo

import (
	"fmt"
	"log"

	"github.com/kelseyhightower/envconfig"
)

var (
	storage *Storage
	config  Config
)

func init() {
	err := envconfig.Process("mongo", &config)
	if err != nil {
		log.Fatalf("process mongo settings: %v", err)
	}

	storage, err = NewStorage(config)
	if err != nil {
		log.Fatalf("create storage: %v", err)
	}
}

func dropDatabase() {
	if err := storage.DropDatabase(); err != nil {
		fmt.Println(err)
	}
}
