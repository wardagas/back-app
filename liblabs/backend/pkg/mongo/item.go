package mongo

import (
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type (
	ItemService struct {
		itemCol   *mgo.Collection
		reviewCol *mgo.Collection
	}
)

func NewItemService(db *mgo.Database,
	itemCollection, reviewCollection string) (*ItemService, error) {
	items := db.C(itemCollection)
	textIndex := mgo.Index{
		Key: []string{"$text:title", "$text:author", "$text:description"},
	}
	if err := items.EnsureIndex(textIndex); err != nil {
		return nil, errors.Wrap(dbError(err), "ensure text index in 'item' collection")
	}

	reviews := db.C(reviewCollection)
	dateIndex := mgo.Index{
		Key: []string{"-date"},
	}
	if err := reviews.EnsureIndex(dateIndex); err != nil {
		return nil, errors.Wrap(dbError(err), "ensure date index in 'review' collection")
	}

	itemUserIndex := mgo.Index{
		Key:      []string{"item_id", "user_id"},
		Unique:   true,
		DropDups: true,
	}
	if err := reviews.EnsureIndex(itemUserIndex); err != nil {
		return nil, errors.Wrap(dbError(err), "ensure index in 'review' collection")
	}
	return &ItemService{itemCol: items, reviewCol: reviews}, nil
}

func (i *ItemService) ByIDList(list []string, offset, limit int) ([]*model.Item, int, error) {
	var items []*model.Item
	query := i.itemCol.Find(bson.M{"_id": bson.M{"$in": list}})

	n, err := query.Count()
	if err != nil {
		return nil, 0, errors.Wrap(dbError(err), "get count of all items")
	}

	if err := query.Skip(offset).Limit(limit).All(&items); err != nil {
		return nil, 0, dbError(err)
	}
	return items, n, nil
}

func (i *ItemService) ByCategory(category string, offset, limit int) ([]*model.Item, int, error) {
	var items []*model.Item
	query := i.itemCol.Find(bson.M{"category_id": category})

	n, err := query.Count()
	if err != nil {
		return nil, 0, errors.Wrap(dbError(err), "get count of all items")
	}

	if err := query.Skip(offset).Limit(limit).All(&items); err != nil {
		return nil, 0, dbError(err)
	}
	return items, n, nil
}

func (i *ItemService) BySlug(slug string) (*model.Item, error) {
	item := new(model.Item)
	if err := i.itemCol.FindId(slug).One(item); err != nil {
		return nil, dbError(err)
	}
	return item, nil
}

func (i *ItemService) Add(items []*model.Item) error {
	insert := make([]interface{}, len(items))

	// Preparations.
	for i := range items {
		insert[i] = interface{}(items[i])
	}

	err := i.itemCol.Insert(insert...)
	return dbError(err)
}

func (i *ItemService) Search(search string, offset, limit int) ([]*model.Item, int, error) {
	var items []*model.Item
	query := i.itemCol.Find(bson.M{
		"$text": bson.M{
			// Add "\"" symbols to search by phrase.
			// https://docs.mongodb.com/manual/reference/operator/query/text/#search-for-a-phrase
			"$search": "\"" + search + "\"",
		}})

	n, err := query.Count()
	if err != nil {
		return nil, 0, errors.Wrap(dbError(err), "get count of all items")
	}

	if err := query.Skip(offset).Limit(limit).All(&items); err != nil {
		return nil, 0, dbError(err)
	}
	return items, n, nil
}

func (i *ItemService) IncCount(slug string, amount int) error {
	err := i.itemCol.UpdateId(slug, bson.M{
		"$inc": bson.M{
			"available_count": amount}})
	return dbError(err)
}

func (i *ItemService) SetScore(slug string, score float32) error {
	err := i.itemCol.UpdateId(slug, bson.M{
		"$set": bson.M{
			"average_score": score}})
	return dbError(err)
}
