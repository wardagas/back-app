package mongo

import (
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/globalsign/mgo"
	"gopkg.in/mgo.v2/bson"
)

type OrderService struct {
	col *mgo.Collection
}

func NewOrderService(db *mgo.Database, collectionName string) (*OrderService, error) {
	collection := db.C(collectionName)
	return &OrderService{collection}, nil
}

func (o *OrderService) AddOrder(order model.Order) error {
	err := o.col.Insert(order)
	return dbError(err)
}

func (o *OrderService) GetOrdersByUserID(id string, offset, limit int) ([]*model.Order, int, error) {
	var orders []*model.Order
	query := o.col.Find(bson.M{"user_id": id})

	n, err := query.Count()
	if err != nil {
		return nil, 0, errors.Wrap(dbError(err), "get count of all orders")
	}

	if err := query.Skip(offset).Limit(limit).All(&orders); err != nil {
		return nil, 0, dbError(err)
	}
	return orders, n, nil
}
