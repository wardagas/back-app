package mongo

import (
	"testing"
	"time"

	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

func TestReviewService(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	defer dropDatabase()

	rs := storage.ItemService
	us := storage.UserService

	users := []*model.User{
		{
			ID:       "kat6123@yandex.ru",
			Name:     "Katerina Shilovskaya",
			ImageURL: "https://dribble.com",
		},
		{
			ID:       "jfxer@gmail.com",
			Name:     "Siyar Akhmad",
			ImageURL: "https://dribble.com",
		},
	}

	reviews := []*model.Review{
		{
			User:    users[0].ID,
			Item:    "garri-potter",
			Comment: "It was great!",
			Date:    time.Date(2018, time.November, 4, 0, 0, 0, 0, time.UTC),
		},
		{
			User:    users[0].ID,
			Item:    "garri-potter",
			Comment: "It was cool!",
			Date:    time.Date(2017, time.November, 4, 0, 0, 0, 0, time.UTC),
		},
		{
			User:    users[1].ID,
			Item:    "garri-potter",
			Comment: "It was sad!",
			Date:    time.Date(2016, time.November, 4, 0, 0, 0, 0, time.UTC),
		},
	}

	for i := range users {
		err := us.Save(*users[i])
		assert.NoError(t, err)
	}

	gotReviews, gotUsers, total, err := rs.ItemReviews("garri-potter", 0, 2)
	assert.NoError(t, err)
	assert.Empty(t, gotReviews)
	assert.Empty(t, gotUsers)
	assert.Zero(t, total)

	err = rs.UpsertReview(reviews[0])
	assert.NoError(t, err)
	err = rs.UpsertReview(reviews[2])
	assert.NoError(t, err)

	err = rs.UpsertReview(reviews[1])
	assert.NoError(t, err)

	gotReviews, gotUsers, total, err = rs.ItemReviews("garri-potter", 0, 2)
	assert.NoError(t, err)
	assert.Equal(t, 2, total)
	assert.Equal(t, reviews[1:], gotReviews)
	assert.Equal(t, users, gotUsers)
}
