package mongo

import (
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/globalsign/mgo/bson"
)

type (
	reviewList struct {
		Total []struct {
			Count int
		}
		Reviews []struct {
			Review model.Review `bson:",inline"`
			User   []model.User
		}
	}
)

func (i *ItemService) UpsertReview(review *model.Review) error {
	_, err := i.reviewCol.Upsert(
		bson.M{"item_id": review.Item, "user_id": review.User}, review)
	return dbError(err)
}

func (i *ItemService) ItemReviews(slug string, offset, limit int) (
	[]*model.Review, []*model.User, int, error) {
	var result reviewList
	pipe := i.reviewCol.Pipe([]bson.M{
		{"$match": bson.M{"item_id": slug}},
		{"$facet": bson.M{
			"total": []bson.M{
				{"$count": "count"},
			},
			"reviews": []bson.M{
				{"$skip": offset},
				{"$limit": limit},
				{"$lookup": bson.M{
					"from":         "user",
					"localField":   "user_id",
					"foreignField": "_id",
					"as":           "user",
				}},
				// Question: why index not applied automatically?
				{"$sort": bson.M{"date": -1}},
			},
		}},
	})
	if err := pipe.One(&result); err != nil {
		return nil, nil, 0, dbError(err)
	}

	// TODO change comment.
	// Match stage doesn't return values, but all stages continue
	// and struct with empty values, even if no one doesn't match.
	count := 0
	if len(result.Total) != 0 {
		count = result.Total[0].Count
	}
	reviews, users := formReviewUserList(result)
	return reviews, users, count, nil
}

// TODO rename to score..
func (i *ItemService) ItemScoreFromReviews(item string) (float32, error) {
	var result struct {
		// Question: float32 or float64?
		AvgScore float32 `bson:"avg_score"`
	}
	pipe := i.reviewCol.Pipe([]bson.M{
		{"$match": bson.M{"item_id": item}},
		{"$group": bson.M{
			"_id":       nil,
			"avg_score": bson.M{"$avg": "$score"},
		},
		},
	})
	if err := pipe.One(&result); err != nil {
		return 0, dbError(err)
	}
	return result.AvgScore, nil
}

func formReviewUserList(list reviewList) ([]*model.Review, []*model.User) {
	reviews := make([]*model.Review, len(list.Reviews))
	users := make([]*model.User, len(list.Reviews))
	for i := range list.Reviews {
		reviews[i] = &list.Reviews[i].Review
		// Question: if user list is empty.
		users[i] = &(list.Reviews[i].User[0])
	}
	return reviews, users
}
