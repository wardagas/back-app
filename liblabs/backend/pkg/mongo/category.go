package mongo

import (
	"bitbucket.org/liblabs/backend/pkg/errors"
	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type (
	CategoryService struct {
		col *mgo.Collection
	}

	category struct {
		model.Category `bson:",inline"`
		Children       []string `bson:"children"`
	}
)

func NewCategoryService(db *mgo.Database, collectionName string) (*CategoryService, error) {
	collection := db.C(collectionName)
	return &CategoryService{collection}, nil
}

func (c *CategoryService) List(offset, limit int) ([]*model.Category, int, error) {
	return c.getChildren("root", offset, limit)
}

func (c *CategoryService) Sub(name string, offset, limit int) ([]*model.Category, int, error) {
	return c.getChildren(name, offset, limit)
}

func (c *CategoryService) getChildren(node string, offset, limit int) ([]*model.Category, int, error) {
	// Get array of children slugs.
	parent := new(category)
	if err := c.col.FindId(node).One(&parent); err != nil {
		return nil, 0, dbError(err)
	}

	var categories []*category
	query := c.col.Find(bson.M{"_id": bson.M{"$in": parent.Children}})

	n, err := query.Count()
	if err != nil {
		return nil, 0, errors.Wrap(dbError(err), "get count of all categories")
	}

	if err := query.Skip(offset).Limit(limit).All(&categories); err != nil {
		return nil, 0, dbError(err)
	}

	result := make([]*model.Category, len(categories))
	for i := range categories {
		result[i] = toCategoryModel(categories[i])
	}
	return result, n, nil
}

func toCategoryModel(c *category) *model.Category {
	category := &c.Category
	if c.Children != nil {
		category.HasSubcategory = true
	}
	return category
}

func (c *CategoryService) Add(categories []*model.Category) error {
	if err := c.insertRootIfNotExists(); err != nil {
		return dbError(err)
	}
	return c.addChildren("root", categories)
}

func (c *CategoryService) insertRootIfNotExists() error {
	err := c.col.FindId("root").One(nil)
	switch {
	case err == mgo.ErrNotFound:
		if err := c.col.Insert(bson.M{"_id": "root"}); err != nil {
			return err
		}
	case err != nil:
		return err
	}
	return nil
}

func (c *CategoryService) AddSubcategories(name string, children []*model.Category) error {
	return c.addChildren(name, children)
}

func (c *CategoryService) addChildren(node string, children []*model.Category) error {
	insert := make([]interface{}, len(children))
	id := make([]string, len(children))

	// Preparations.
	for i := range children {
		insert[i] = interface{}(children[i])
		id[i] = children[i].Slug
	}

	// Question: how to insert many?
	if err := c.col.Insert(insert...); err != nil {
		return dbError(err)
	}

	err := c.col.UpdateId(node,
		bson.M{"$addToSet": bson.M{"children": bson.M{"$each": id}}})
	return dbError(err)
}

func (c *CategoryService) Upsert(category model.Category) error {
	_, err := c.col.UpsertId(category.Slug, bson.M{"$set": category})
	return dbError(err)
}

func (c *CategoryService) bySlug(slug string) (*model.Category, error) {
	cg := new(category)
	if err := c.col.FindId(slug).One(&cg); err != nil {
		return nil, dbError(err)
	}
	return toCategoryModel(cg), nil
}
