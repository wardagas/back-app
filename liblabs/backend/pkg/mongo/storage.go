package mongo

import (
	"fmt"

	"github.com/globalsign/mgo"
)

type (
	Config struct {
		URI      string `envconfig:"URI"`
		Database string `envconfig:"DB"`
	}

	Storage struct {
		*mgo.Database
		*UserService
		*CategoryService
		*ItemService
		*OrderService
	}
)

func NewStorage(mongo Config) (*Storage, error) {
	s, err := mgo.Dial(mongo.URI)
	if err != nil {
		return nil, fmt.Errorf("dial %s failed: %v", mongo.URI, err)
	}

	// TODO add Database name as parameter.
	db := s.DB(mongo.Database)
	us, err := NewUserService(db, "user", "token")
	if err != nil {
		return nil, fmt.Errorf("create user storage service: %v", err)
	}

	cs, err := NewCategoryService(db, "category")
	if err != nil {
		return nil, fmt.Errorf("create category storage service: %v", err)
	}

	is, err := NewItemService(db, "item", "review")
	if err != nil {
		return nil, fmt.Errorf("create item storage service: %v", err)
	}

	os, err := NewOrderService(db, "order")
	if err != nil {
		return nil, fmt.Errorf("create order storage service: %v", err)
	}

	return &Storage{
		Database:        db,
		ItemService:     is,
		UserService:     us,
		CategoryService: cs,
		OrderService:    os,
	}, nil
}
