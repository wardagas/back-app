package mongo

import (
	"fmt"

	"bitbucket.org/liblabs/backend/pkg/errors"
	"github.com/globalsign/mgo"
)

func dbError(err error) error {
	if err != nil {
		var kind errors.Kind

		switch {
		case err == mgo.ErrNotFound:
			kind = errors.NotFound
		case mgo.IsDup(err):
			kind = errors.Duplicate
		default:
			kind = errors.Other
		}

		err = &errors.Error{
			Kind: kind,
			Err:  fmt.Errorf("database: %v", err),
		}
	}
	return err
}
