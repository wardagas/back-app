package mongo

import (
	"testing"
	"time"

	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

func TestOrderService(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	defer dropDatabase()

	orders := []*model.Order{
		{
			User:     "kat6123@yandex.ru",
			Slug:     "garri-potter",
			Title:    "Garri Potter",
			ImageURL: "http://dribble.com",
			Date: model.OrderTime{
				Time: time.Date(2018, time.November,
					4, 0, 0, 0, 0, time.UTC)},
			Status: "pending",
		},
		{
			User:     "kat6123@yandex.ru",
			Slug:     "anna-karenina",
			Title:    "Anna Karenina",
			ImageURL: "http://dribble.com",
			Date: model.OrderTime{
				Time: time.Date(2017, time.November,
					4, 0, 0, 0, 0, time.UTC)},
			Status: "returned",
		},
	}
	for i := range orders {
		err := storage.AddOrder(*orders[i])
		assert.NoError(t, err)
	}

	gotOrders, n, err := storage.GetOrdersByUserID("kat6123@yandex.ru", 0, 2)
	assert.NoError(t, err)
	assert.Equal(t, 2, n)
	assert.Equal(t, orders, gotOrders)
}
