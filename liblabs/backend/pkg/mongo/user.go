package mongo

import (
	"fmt"

	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/globalsign/mgo"
	"gopkg.in/mgo.v2/bson"
)

type (
	UserService struct {
		userCol  *mgo.Collection
		tokenCol *mgo.Collection
	}
)

func NewUserService(db *mgo.Database,
	userCollection, tokenCollection string) (*UserService, error) {
	users := db.C(userCollection)
	index := mgo.Index{
		Key:        []string{"email"},
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	// Question: ensure index or drop and create new one?
	if err := users.EnsureIndex(index); err != nil {
		return nil, fmt.Errorf("ensure user collection index: %v", err)
	}

	tokens := db.C(tokenCollection)
	return &UserService{users, tokens}, nil
}

func (s *UserService) ByID(id string) (*model.User, error) {
	p := new(model.User)
	err := s.userCol.FindId(id).One(&p)
	return p, dbError(err)
}

func (s *UserService) Save(u model.User) error {
	err := s.userCol.Insert(u)
	return dbError(err)
}

func (s *UserService) SetQR(id string, qr string) error {
	err := s.userCol.UpdateId(id, bson.M{
		"$set": bson.M{
			"qr": qr}})
	return dbError(err)
}

func (s *UserService) AddFavorite(id, favorite string) error {
	err := s.userCol.UpdateId(id, bson.M{
		"$addToSet": bson.M{
			"favorites": favorite}})
	return dbError(err)
}

func (s *UserService) DeleteFavorite(id, favorite string) error {
	err := s.userCol.UpdateId(id, bson.M{
		"$pull": bson.M{
			"favorites": favorite}})
	return dbError(err)
}

func (s *UserService) IncBalance(id string, amount int) error {
	err := s.userCol.UpdateId(id, bson.M{
		"$inc": bson.M{
			"balance": amount}})
	return dbError(err)
}

func (s *UserService) AddPaymentToken(token, userID string) error {
	err := s.tokenCol.Insert(bson.M{"_id": token, "user_id": userID})
	return dbError(err)
}

func (s *UserService) PaymentToken(id string) (*model.PaymentToken, error) {
	token := new(model.PaymentToken)
	err := s.tokenCol.FindId(id).One(token)
	return token, dbError(err)
}

func (s *UserService) RemovePaymentToken(token string) error {
	err := s.tokenCol.RemoveId(token)
	return dbError(err)
}
