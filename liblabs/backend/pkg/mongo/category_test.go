package mongo

import (
	"testing"

	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/stretchr/testify/assert"
)

func TestCategoryService(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	cs := storage.CategoryService
	defer dropDatabase()

	categories := map[string][]*model.Category{
		"top": {
			{
				Slug:           "fantasy",
				Title:          "fantasy",
				ImageURL:       "https://dribbble.com",
				HasSubcategory: true,
			},
		},
		"fantasy_children": {
			{
				Slug:           "fiction",
				Title:          "fiction",
				ImageURL:       "https://dribbble.com",
				HasSubcategory: false,
			},
			{
				Slug:           "new",
				Title:          "new",
				ImageURL:       "https://dribbble.com",
				HasSubcategory: false,
			},
			{
				Slug:           "science-fantasy",
				Title:          "science-fantasy",
				ImageURL:       "https://dribbble.com",
				HasSubcategory: false,
			},
		},
	}

	// Test work with list of categories.
	err = cs.Add(categories["top"])
	assert.NoError(t, err, "want no errors: add top categories")

	// Test duplicate error.
	err = cs.Add(categories["top"])
	assert.EqualError(t, err,
		"database: E11000 duplicate key error collection: "+
			"test.category index: _id_ dup key: { : \"fantasy\" }")

	err = cs.AddSubcategories("fantasy", categories["fantasy_children"])
	assert.NoError(t, err, "want no errors: add subcategories to fantasy")

	top, n, err := cs.List(0, 1)
	assert.NoError(t, err, "want no errors: get top categories")
	assert.Equal(t, categories["top"], top)
	// Expected number of top categories without the use of limit and offset.
	assert.Equal(t, 1, n)

	sub, n, err := cs.Sub("fantasy", 0, 2)
	assert.NoError(t, err, "want no errors: get sub categories")
	// Results are returned in alphabet order.
	assert.Equal(t, categories["fantasy_children"][0:2], sub)
	assert.Equal(t, 3, n)
}

func TestCategoryService_Upsert(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	cs := storage.CategoryService
	defer dropDatabase()

	fantasy := &model.Category{
		Slug:     "fantasy",
		Title:    "fantasy",
		ImageURL: "https://dribbble.com",
	}

	// Test insert.
	err = cs.Upsert(*fantasy)
	assert.NoError(t, err)

	gotFantasy, err := cs.bySlug("fantasy")
	assert.NoError(t, err)
	assert.Equal(t, fantasy, gotFantasy)

	// Test update.
	fantasy.ImageURL = "https://notdribble.com"
	err = cs.Upsert(*fantasy)
	assert.NoError(t, err)

	gotFantasy, err = cs.bySlug("fantasy")
	assert.NoError(t, err)
	assert.Equal(t, fantasy, gotFantasy)
}

func TestCategoryServiceErrors(t *testing.T) {
	storage, err := NewStorage(config)
	assert.NoError(t, err)
	cs := storage.CategoryService
	defer dropDatabase()

	// Test get by slug of not existing category.
	_, err = cs.bySlug("fantasy")
	assert.EqualError(t, err, "database: not found")
}
