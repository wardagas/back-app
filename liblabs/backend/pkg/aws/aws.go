package aws

import (
	"bytes"
	"fmt"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/endpoints"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type (
	Config struct {
		Key    string `envconfig:"key"`
		Secret string `envconfig:"secret"`
		Domain string `envconfig:"domain"`
	}

	Service struct {
		session *session.Session
		domain  string
	}
)

func New(config Config) (*Service, error) {
	// TODO check if session should be closed.
	s, err := session.NewSession(&aws.Config{
		Region: aws.String(endpoints.UsWest2RegionID),
		Credentials: credentials.NewStaticCredentials(
			config.Key, config.Secret, "")})
	if err != nil {
		return nil, fmt.Errorf("create session to aws service: %v", err)
	}

	return &Service{
		session: s,
		domain:  config.Domain,
	}, nil
}

func (s *Service) UploadImage(bucket, path string, image []byte) error {
	_, err := s3.New(s.session).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(bucket),
		Key:                  aws.String(path),
		ACL:                  aws.String("public-read"),
		Body:                 bytes.NewReader(image),
		ContentLength:        aws.Int64(int64(len(image))),
		ContentType:          aws.String(http.DetectContentType(image)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
	})
	return err
}

func (s *Service) ImageURL(bucket, path string) string {
	// TODO fix this
	return s.domain + "/" + bucket + "/" + path
}
