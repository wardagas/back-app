package bepaid

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/liblabs/backend/pkg/model"
)

type (
	Config struct {
		ID        string `envconfig:"id"`
		SecretKey string `envconfig:"secret_key"`
		Success   string `envconfig:"success"`
		Decline   string `envconfig:"decline"`
		Fail      string `envconfig:"fail"`
		Hook      string `envconfig:"hook"`
	}

	BePaid struct {
		config Config
		client *http.Client
	}

	Error struct {
		Message string `json:"message"`
	}
)

func New(config Config) *BePaid {
	return &BePaid{
		config: config,
		client: &http.Client{
			Timeout: time.Minute,
		},
	}
}

// RequestToken creates payment token and redirect url using
// information about customer and order.
func (b *BePaid) RequestToken(o model.Points, c model.Customer) (*model.Token, error) {
	const tokenRequestURL = "https://checkout.bepaid.by/ctp/api/checkouts"

	var request = map[string]interface{}{
		"checkout": map[string]interface{}{
			"version":          2.1,
			"test":             true,
			"transaction_type": "payment",
			"attempts":         1,
			"settings": map[string]interface{}{
				"success_url":      b.config.Success,
				"decline_url":      b.config.Decline,
				"fail_url":         b.config.Fail,
				"notification_url": b.config.Hook,
				"language":         "ru",
				"customer_fields": map[string]interface{}{
					"visible": []string{"first_name", "last_name"},
				},
			},
			"order":    o,
			"customer": c,
		},
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(&request); err != nil {
		return nil, fmt.Errorf("encode request: %v", err)
	}
	req, err := http.NewRequest(http.MethodPost, tokenRequestURL, &buf)
	if err != nil {
		return nil, fmt.Errorf("create request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	req.SetBasicAuth(b.config.ID, b.config.SecretKey)
	resp, err := b.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("make request: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		error := new(Error)
		if err := json.NewDecoder(resp.Body).Decode(&error); err != nil {
			return nil, fmt.Errorf("decode json: %v", err)
		}
		return nil, fmt.Errorf("status %d: %s", resp.StatusCode, error.Message)
	}

	var result struct {
		Checkout *model.Token `json:"checkout"`
	}
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, fmt.Errorf("decode json: %v", err)
	}
	return result.Checkout, nil
}
