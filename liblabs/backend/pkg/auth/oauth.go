package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"strconv"

	"bitbucket.org/liblabs/backend/pkg/model"
	"golang.org/x/oauth2"
	oauthgoogle "golang.org/x/oauth2/google"
	oauthvk "golang.org/x/oauth2/vk"
)

const (
	googleUserInfoAPI = "https://www.googleapis.com/oauth2/v3/userinfo"
	vkUserInfoAPI     = "api.VK.com/method/users.get"
	vkVersion         = "5.85"
	vkImageField      = "photo_100"
)

type (
	Config struct {
		Google Credentials
		VK     Credentials

		JWTSecret string `envconfig:"JWTSECRET"`
	}

	Credentials struct {
		ID          string
		Secret      string
		RedirectURI string
	}

	vkResponse struct {
		ID        uint64 `json:"id"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		ImageURL  string `json:"photo_100"`
	}

	vkError struct {
		Code int    `json:"error_code"`
		Msg  string `json:"error_msg"`
	}

	googleResponse struct {
		Name    string `json:"name"`
		Email   string `json:"email"`
		Picture string `json:"picture"`
	}

	OAuthService struct {
		google *oauth2.Config
		vk     *oauth2.Config
	}
)

func NewOauth(config Config) *OAuthService {
	google := &oauth2.Config{
		ClientID:     config.Google.ID,
		ClientSecret: config.Google.Secret,
		// TODO add redirection url
		RedirectURL: config.Google.RedirectURI,
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: oauthgoogle.Endpoint,
	}
	vk := &oauth2.Config{
		ClientID:     config.VK.ID,
		ClientSecret: config.VK.Secret,
		RedirectURL:  config.VK.RedirectURI,
		Scopes: []string{
			"email",
		},
		Endpoint: oauthvk.Endpoint,
	}
	return &OAuthService{
		google: google,
		vk:     vk,
	}
}

func (o *OAuthService) VKToken(code string) (*oauth2.Token, error) {
	ctx := context.Background()
	tok, err := o.vk.Exchange(ctx, code)
	if err != nil {
		return nil, err
	}
	return tok, nil
}

func (o *OAuthService) GoogleToken(code string) (*oauth2.Token, error) {
	ctx := context.Background()
	tok, err := o.google.Exchange(ctx, code)
	if err != nil {
		return nil, err
	}
	return tok, nil
}

func (o *OAuthService) GoogleUser(tok *oauth2.Token) (*model.User, error) {
	client := o.google.Client(context.Background(), tok)
	body, err := client.Get(googleUserInfoAPI)
	if err != nil {
		return nil, err
	}
	defer body.Body.Close()

	var response googleResponse
	if err := json.NewDecoder(body.Body).Decode(&response); err != nil {
		return nil, fmt.Errorf("decode google response: %v", err)
	}

	u := new(model.User)
	u.ID = response.Email
	u.Name = response.Name
	u.ImageURL = response.Picture
	return u, nil
}

func (o *OAuthService) VKUser(tok *oauth2.Token) (*model.User, error) {
	client := o.vk.Client(context.Background(), tok)
	body, err := client.Get(getVKURL(tok.AccessToken))
	if err != nil {
		return nil, err
	}
	defer body.Body.Close()

	buf, err := ioutil.ReadAll(body.Body)
	if err != nil {
		return nil, err
	}

	var response struct {
		Response []vkResponse `json:"response"`
	}
	if err := json.Unmarshal(buf, &response); err != nil {
		return nil, fmt.Errorf("decode vk response: %v", err)
	}

	// VK api returns http.StatusOK even if error occurs.
	// So we can define error by response body.
	if len(response.Response) < 1 {
		return nil, getVKResponseError(buf)
	}

	vkUser := response.Response[0]
	u := new(model.User)
	u.ID = strconv.FormatUint(vkUser.ID, 10)
	u.Name = vkUser.FirstName + " " + vkUser.LastName
	u.ImageURL = vkUser.ImageURL
	return u, nil
}

func getVKURL(accessToken string) string {
	qs := url.Values{
		"fields":       []string{vkImageField},
		"v":            []string{vkVersion},
		"access_token": []string{accessToken},
	}
	url := url.URL{
		Scheme:   "https",
		Path:     vkUserInfoAPI,
		RawQuery: qs.Encode(),
	}
	return url.String()
}

func getVKResponseError(buf []byte) error {
	var error struct {
		Err vkError `json:"error"`
	}
	if err := json.Unmarshal(buf, &error); err != nil {
		return fmt.Errorf("decode vk response error: %v", err)
	}
	// TODO add http error to response
	return fmt.Errorf("vk code %d: msg %q", error.Err.Code, error.Err.Msg)
}
