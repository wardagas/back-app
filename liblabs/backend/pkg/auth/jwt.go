package auth

import (
	"fmt"
	"time"

	"bitbucket.org/liblabs/backend/pkg/model"
	"github.com/dgrijalva/jwt-go"
)

var jwtSecret []byte

func InitJWT(secret string) {
	jwtSecret = []byte(secret)
}

func JWT(u model.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.MapClaims{
			"exp":  time.Now().Add(time.Hour * 8760).Unix(),
			"name": u.Name,
			"id":   u.ID,
		})
	return token.SignedString(jwtSecret)
}

func KeyJWT() func(*jwt.Token) (interface{}, error) {
	return func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return jwtSecret, nil
	}
}

func UserFromJWT(token *jwt.Token) (model.User, error) {
	user := new(model.User)
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return model.User{}, fmt.Errorf("claims are not of standard type")
	}

	user.Name = claims["name"].(string)
	user.ID = claims["id"].(string)
	return *user, nil
}
