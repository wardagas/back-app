package config

import (
	"fmt"

	"bitbucket.org/liblabs/backend/pkg/auth"
	"bitbucket.org/liblabs/backend/pkg/aws"
	"bitbucket.org/liblabs/backend/pkg/bepaid"
	"bitbucket.org/liblabs/backend/pkg/handler/middleware"
	"bitbucket.org/liblabs/backend/pkg/mongo"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Mongo       mongo.Config
	CORS        middleware.CORSConfig
	BePaid      bepaid.Config
	OAuthWeb    auth.Config
	OAuthMobile auth.Config
	AWS         aws.Config
}

func New() (*Config, error) {
	config := new(Config)

	var m mongo.Config
	err := envconfig.Process("mongo", &m)
	if err != nil {
		return nil, fmt.Errorf("process mongo settings: %v", err)
	}
	config.Mongo = m

	var c middleware.CORSConfig
	err = envconfig.Process("allowed", &c)
	if err != nil {
		return nil, fmt.Errorf("process cors settings: %v", err)
	}
	config.CORS = c

	var p bepaid.Config
	err = envconfig.Process("payment", &p)
	if err != nil {
		return nil, fmt.Errorf("process payment settings: %v", err)
	}
	config.BePaid = p

	var aws aws.Config
	err = envconfig.Process("aws", &aws)
	if err != nil {
		return nil, fmt.Errorf("process aws settings: %v", err)
	}
	config.AWS = aws

	a, err := readAuth("web")
	if err != nil {
		return nil, err
	}
	config.OAuthWeb = *a

	a, err = readAuth("mobile")
	if err != nil {
		return nil, err
	}
	config.OAuthMobile = *a
	return config, nil
}

func readAuth(kind string) (*auth.Config, error) {
	var a auth.Config
	err := envconfig.Process(kind+"_google", &a.Google)
	if err != nil {
		return nil, fmt.Errorf("process google settings: %v", err)
	}
	err = envconfig.Process(kind+"_vk", &a.VK)
	if err != nil {
		return nil, fmt.Errorf("process vk settings: %v", err)
	}
	err = envconfig.Process("", &a)
	if err != nil {
		return nil, fmt.Errorf("process jwt settings: %v", err)
	}
	return &a, nil
}
