package errors

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	// NotFound error.
	NotFound Kind = iota
	// Duplicate error.
	Duplicate
	Conflict
	// Other is the kind of not classified error.
	Other
)

type (
	// Kind of error.
	Kind int

	// Error is a special type of error which is used by tour microservice.
	Error struct {
		Kind Kind
		Err  error
	}

	message struct {
		Content string `json:"error"`
	}
)

func (e *Error) Error() string {
	return fmt.Sprintf("%v", e.Err)
}

func IsNotFound(e error) bool {
	t, ok := e.(*Error)
	return ok && t.Kind == NotFound
}

// Wrap return new error which was wrapped by s string.
// If e is of type Error then Err field will be wrapped.
func Wrap(e error, w string) error {
	t, ok := e.(*Error)
	if ok {
		t.Err = fmt.Errorf(w+": %v", t.Err)
		return t
	}
	return fmt.Errorf(w+": %v", e)
}

// HTTPStatus return status based on error type.
func HTTPStatus(e error) int {
	t, ok := e.(*Error)
	if ok {
		switch t.Kind {
		case NotFound:
			return http.StatusNotFound
		case Duplicate:
			return http.StatusBadRequest
		case Conflict:
			return http.StatusConflict
		default:
			return http.StatusInternalServerError
		}
	}
	return http.StatusInternalServerError
}

func WriteJSON(w http.ResponseWriter, errMsg string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)

	if err := json.NewEncoder(w).Encode(message{errMsg}); err != nil {
		panic(fmt.Sprintf("encode error to json has failed: %v", err))
	}
}
